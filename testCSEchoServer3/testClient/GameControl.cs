﻿using System;
namespace testClient
{
	public class GameControl
	{
		public World world;

		public GameControl()
		{
		}
		public ConsoleKey consoleKey = ConsoleKey.NoName;

		public void Run()
		{
			
			var key = Console.ReadKey();
			Console.WriteLine(key.Key);
			consoleKey = key.Key;

		}
		public void UpdateBody()
		{
			var myBody = world.GetMyBody();
			var key = consoleKey;
			var speed = 10;
			if (key == ConsoleKey.LeftArrow)
			{
				myBody.x -= speed;
			}
			else if (key == ConsoleKey.RightArrow)
			{
				myBody.x += speed;
			}
			else if (key == ConsoleKey.UpArrow)
			{
				myBody.y -= speed;
			}
			else if (key == ConsoleKey.DownArrow)
			{
				myBody.y += speed;
			}
			Console.WriteLine("ReadKey:"+key);
			consoleKey = ConsoleKey.NoName;
		}
	}
}

