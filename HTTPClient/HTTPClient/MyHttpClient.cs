﻿using System;
using System.Net.Http;
using System.Threading.Tasks;


namespace HTTPClient
{
	public class MyHttpClient
	{

		public MyHttpClient()
		{
			var t1 = Task.Run(SendCommand);
			//t1.Wait();
			/*
			var t =  Task.Factory.StartNew(DownloadPage);
			var ta = t.Result;
			ta.Wait();
			*/

			var t2 = Task.Run(Receive);
			t2.Wait();
		}
		private async Task SendCommand()
		{
			while (true)
			{
				var con = await Task.Run(() => Console.ReadLine());

				var page = "http://localhost:9090/send?" + con;
				using (var client = new HttpClient())
				using (var resp = await client.GetAsync(page))
				using (var content = resp.Content)
				{
					Console.WriteLine("Send:" + con);
				}
			}
		}

		public async Task Receive()
		{
			while (true)
			{
				var page = "http://localhost:9090/receive";
				using (var client = new HttpClient())
				using (var resp = await client.GetAsync(page))
				using (var content = resp.Content)
				{
					var result = await content.ReadAsStringAsync();
					Console.WriteLine("Receive:"+result);
				}
			}
		}


		//Task1：读取命令 s 数据 给发送聊天数据给服务器
		//Task2：服务器上 获取聊天数据
		private async Task DownloadPage()
		{
			Console.WriteLine("StartRequest");
			var page = "http://localhost:9090";
			using (var client = new HttpClient())
			using (var resp = await client.GetAsync(page))
			using (var content = resp.Content)
			{
				var result = await content.ReadAsStringAsync();
				Console.WriteLine(result);
			}
			Console.WriteLine("FinishRequest");
		}
	}
}
